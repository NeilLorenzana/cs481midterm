﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Microcharts;
using SkiaSharp;

namespace CS481Midterm.Models
{
    public static class StockDataModel
    {

        const string API_KEY = "&apikey=1FYVI4XC3ZSW8DBV";
        const string END_POINT = "https://www.alphavantage.co/query?function=TIME_SERIES_DAILY&symbol=";

        static public string lastSymbol;

        static Dictionary<string, TimeSeriesDaily> dailyData;


        public static async Task<Dictionary<string, TimeSeriesDaily>> GetSymbolData(string symbol)
        {
           
            if (symbol != lastSymbol)
            {
                //Pulls Data
                Uri stockApiUri = new Uri(END_POINT + symbol + API_KEY);

                HttpClient client = new HttpClient();

                HttpResponseMessage response = await client.GetAsync(stockApiUri);

                if (response.IsSuccessStatusCode)
                {
                    
                    string jsonContent = await response.Content.ReadAsStringAsync();
                    dailyData = StocksJsonModel.FromJson(jsonContent).TimeSeriesDaily;

                    if (dailyData == null)
                    {
                        return null;
                    }
                }
                lastSymbol = symbol;
            }
            return dailyData;
        }

        public static string GetHighest()
        {
            double stockHigh = double.MinValue;
            if (dailyData != null)
            {
                foreach (KeyValuePair<string, TimeSeriesDaily> item in dailyData)
                {
                    double dailyHigh = item.Value.The2High;
                    if (dailyHigh > stockHigh) stockHigh = dailyHigh;
                }
                return "Highest: " + stockHigh.ToString("c2");
            }
            return "";
        }

        public static string GetLowest()
        {
            double stockLow = double.MaxValue;
            if (dailyData != null)
            {
                foreach (KeyValuePair<string, TimeSeriesDaily> item in dailyData)
                {
                    double dailyLow = item.Value.The2High;
                    if (dailyLow < stockLow) stockLow = dailyLow;
                }
                return "Lowest: " + stockLow.ToString("c2");
            }
            return "";
        }

        public static List<Entry> GetPastDayRange(int range)
        {
            var subList = GetAsEntries();
            subList.Reverse();
            if (subList.Count > range)
            {
                return subList.GetRange(0, range);
            }
            return subList;
        }

        private static List<Entry> GetAsEntries()
        {
            var entryList = new List<Entry>();
            if (dailyData != null)
            {
                int i = 0;
                foreach (KeyValuePair<string, TimeSeriesDaily> item in dailyData)
                {
                    var newEntry = new Entry((float)item.Value.The2High)
                    {
                        TextColor = SKColors.White,
                        Color = SKColors.White
                    };
                    if (i % 5 == 0)
                    {
                        newEntry.ValueLabel = item.Value.The2High.ToString();
                    }
                    entryList.Add(newEntry);
                    i++;
                }
            }
 
            return entryList;
        }
    }
}