﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using CS481Midterm.Models;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]
namespace CS481Midterm
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();
            MainPage = new MainPage();
        }

        protected override void OnStart()
        {
            
        }

        protected override void OnSleep()
        {
            
        }

        protected override void OnResume()
        {

        }
    }
}
